<?php declare(strict_types = 1);

namespace Abetzi\SmsManager;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Notification;
use InvalidArgumentException;
use Throwable;

class SmsManagerChannel
{

	/** @var string */
	private $apiKey;

	/** @var GuzzleClient */
	private $guzzleClient;

	public function __construct(GuzzleClient $guzzleClient, string $apiKey)
	{
		$this->guzzleClient = $guzzleClient;
		$this->apiKey       = $apiKey;
	}

	/**
	 * @param               $notifiable
	 * @param  Notification $notification
	 *
	 * @return array
	 * @throws Throwable
	 */
	public function send($notifiable, $notification): array
	{
		throw_unless(
			method_exists($notification, 'toSmsManager'),
			new InvalidArgumentException('Notification class does not contain toSmsManager method. Please, implement it.')
		);

		$message = $notification->toSmsManager($notifiable);

		throw_unless(
			$message instanceof SmsMessage,
			new InvalidArgumentException('Not recognized notification type, it should be SmsMessage class.')
		);

		$route = $this->route($notifiable);

		try {
			$response = $this->guzzleClient->get('/Send', [
				'query' => [
					'apikey'  => $this->apiKey,
					'number'  => $route,
					'message' => $message->getBody(),
					'gateway' => $message->getGateway(),
				],
			]);

			$responseString = $response->getBody()->getContents();
			$data           = explode('|', $responseString);

			return [
				'status'      => $data[0],
				'requestId'   => $data[1],
				'phoneNumber' => $data[2],
				'customId'    => $data[3] ?? null,
			];
		} catch (ClientException | ServerException $e) {
			$responseString = $e->getResponse()->getBody()->getContents();
			$data           = explode('|', $responseString);

			return [
				'status' => $data[0],
				'code'   => $data[1],
			];
		}
	}

	/**
	 * @param $notifiable
	 *
	 * @return mixed
	 * @throws Throwable
	 */
	private function route($notifiable)
	{
		if ($notifiable instanceof AnonymousNotifiable) {
			throw_unless(
				Arr::has($notifiable->routes, self::class),
				new InvalidArgumentException('Anonymous notifiable must contains a route for sms manager channel.')
			);

			return $notifiable->routes[self::class];
		}

		throw_unless(
			method_exists($notifiable, 'routeNotificationForSmsManager'),
			new InvalidArgumentException('Notifiable class does not contain routeNotificationForSmsManager method. Please, implement it.')
		);

		return $notifiable->routeNotificationFor('SmsManager');
	}
}
