<?php declare(strict_types = 1);

namespace Abetzi\SmsManager;

use Illuminate\Support\ServiceProvider;

class SmsManagerServiceProvider extends ServiceProvider
{

	public function register()
	{
		$this->app->bind(SmsManagerChannel::class, function () {
			$apiKey     = config('smsmanager.apikey');
			$httpClient = new \GuzzleHttp\Client([
				'base_uri' => config('smsmanager.base_uri'),
				'timeout'  => config('timeout'),
			]);

			return new SmsManagerChannel($httpClient, $apiKey);
		});
	}

	public function boot()
	{
		$this->publishes([
			__DIR__ . '/smsmanager.php' => config_path('smsmanager.php'),
		]);
	}
}
