<?php declare(strict_types = 1);

namespace Abetzi\SmsManager;

class SmsMessage
{

	/** @var string */
	private $body;

	/** @var string */
	private $gateway;

	public function __construct(string $body, string $gateway = Gateway::LOWCOST)
	{
		$this->body    = $body;
		$this->gateway = $gateway;
	}

	public function getBody(): string
	{
		return $this->body;
	}

	public function getGateway(): string
	{
		return $this->gateway;
	}
}
