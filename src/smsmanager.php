<?php

return [
	'apikey'   => env('SMSMANAGER_APIKEY', ''),
	'base_uri' => env('SMSMANAGER_HTTPAPI', 'https://http-api.smsmanager.cz'),
	'timeout'  => env('SMSMANAGER_TIMEOUT', 30.0),
];
