<?php declare(strict_types = 1);

namespace Abetzi\SmsManager;

class Gateway
{

	public const HIGH = 'high';

	public const ECONOMY = 'economy';

	public const LOWCOST = 'lowcost';
}
