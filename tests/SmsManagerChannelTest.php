<?php declare(strict_types = 1);

namespace Abetzi\SmsManagerTest;

use Abetzi\SmsManager\SmsManagerChannel;
use Abetzi\SmsManagerTest\Fixtures\NotificationWithoutRoute;
use Abetzi\SmsManagerTest\Fixtures\RightNotification;
use Abetzi\SmsManagerTest\Fixtures\RightUser;
use Abetzi\SmsManagerTest\Fixtures\UserWithouRoute;
use GuzzleHttp\Psr7\Response;
use Illuminate\Notifications\AnonymousNotifiable;

class SmsManagerChannelTest extends \PHPUnit\Framework\TestCase
{

	/**
	 * @test
	 */
	public function itSuccessfulySendsASmsMessage()
	{
		$client = $this->createClientWithResponses([
			new Response(200, [], 'OK|12345|420603123456'),
		]);

		$api          = 'secret api';
		$notifiable   = new RightUser();
		$notification = new RightNotification();

		$channel = new SmsManagerChannel($client, $api);
		$result  = $channel->send($notifiable, $notification);

		self::assertEquals('OK', $result['status']);
		self::assertEquals($notifiable->phone_number, $result['phoneNumber']);
	}

	/**
	 * @test
	 */
	public function itSuccessfulySendsASmsMessageToAnAnonymousNotifiable()
	{
		$client = $this->createClientWithResponses([
			new Response(200, [], 'OK|12345|420603123456'),
		]);

		$phoneNumber  = '420603123456';
		$api          = 'secret api';
		$notifiable   = (new AnonymousNotifiable())->route(SmsManagerChannel::class, $phoneNumber);
		$notification = new RightNotification();

		$channel = new SmsManagerChannel($client, $api);
		$result  = $channel->send($notifiable, $notification);

		self::assertEquals('OK', $result['status']);
		self::assertEquals($phoneNumber, $result['phoneNumber']);
	}

	/**
	 * @test
	 */
	public function itFailsWhenNotificationDoesNotContainsToSmsManagerMethod()
	{
		// Given
		$client = $this->createClientWithResponses([
			new Response(200, [], 'OK|12345|420603123456'),
		]);

		$api          = 'secret api';
		$notifiable   = new UserWithouRoute();
		$notification = new NotificationWithoutRoute();

		$channel = new SmsManagerChannel($client, $api);

		// Exception expected
		$this->expectException(\InvalidArgumentException::class);

		// When
		$channel->send($notifiable, $notification);
	}

	/**
	 * @test
	 */
	public function itFailsWhenMessageIsNotCorrectType()
	{
		// Given
		$client = $this->createClientWithResponses([
			new Response(200, [], 'OK|12345|420603123456'),
		]);

		$api          = 'secret api';
		$notifiable   = new UserWithouRoute();
		$notification = new NotificationWithoutRoute();

		$channel = new SmsManagerChannel($client, $api);

		// Exception expected
		$this->expectException(\InvalidArgumentException::class);

		// When
		$channel->send($notifiable, $notification);
	}

	/**
	 * @test
	 */
	public function itFailsWhenNotifiableDoesNotRouteToSmsManager()
	{
		// Given
		$client = $this->createClientWithResponses([
			new Response(200, [], 'OK|12345|420603123456'),
		]);

		$api          = 'secret api';
		$notifiable   = new UserWithouRoute();
		$notification = new RightNotification();

		$channel = new SmsManagerChannel($client, $api);

		// Exception expected
		$this->expectException(\InvalidArgumentException::class);

		// When
		$channel->send($notifiable, $notification);
	}

	/**
	 * @test
	 */
	public function itFailsWhenAnonymousNotifiableDoesNotHaveSmsManagerRoute()
	{
		// Given
		$client = $this->createClientWithResponses([
			new Response(200, [], 'OK|12345|420603123456'),
		]);

		$api          = 'secret api';
		$notifiable   = new AnonymousNotifiable();
		$notification = new RightNotification();

		$channel = new SmsManagerChannel($client, $api);

		// Exception expected
		$this->expectException(\InvalidArgumentException::class);

		// When
		$channel->send($notifiable, $notification);
	}

	/**
	 * @test
	 */
	public function itReturnsErrorDataAfterClientError()
	{
		$client = $this->createClientWithResponses([
			new Response(400, [], 'ERROR|101'),
		]);

		$api          = 'secret api';
		$notifiable   = new RightUser();
		$notification = new RightNotification();

		$channel = new SmsManagerChannel($client, $api);
		$result  = $channel->send($notifiable, $notification);

		self::assertEquals('ERROR', $result['status']);
	}

	/**
	 * @test
	 */
	public function itReturnsErrorDataAfterServerError()
	{
		$client = $this->createClientWithResponses([
			new Response(500, [], 'ERROR|901'),
		]);

		$api          = 'secret api';
		$notifiable   = new RightUser();
		$notification = new RightNotification();

		$channel = new SmsManagerChannel($client, $api);
		$result  = $channel->send($notifiable, $notification);

		self::assertEquals('ERROR', $result['status']);
	}

	private function createClientWithResponses(array $responses)
	{
		$mock    = new \GuzzleHttp\Handler\MockHandler($responses);
		$handler = \GuzzleHttp\HandlerStack::create($mock);

		return new \GuzzleHttp\Client(['handler' => $handler]);
	}
}
