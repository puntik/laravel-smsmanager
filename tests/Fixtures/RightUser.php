<?php declare(strict_types = 1);

namespace Abetzi\SmsManagerTest\Fixtures;

use Illuminate\Database\Eloquent\Model;

class RightUser extends Model
{

	use \Illuminate\Notifications\Notifiable;

	public function getPhoneNumberAttribute()
	{
		return '420603123456';
	}

	public function routeNotificationForSmsManager()
	{
		return $this->phone_number;
	}
}
