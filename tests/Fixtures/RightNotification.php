<?php declare(strict_types = 1);

namespace Abetzi\SmsManagerTest\Fixtures;

use Abetzi\SmsManager\SmsManagerChannel;
use Abetzi\SmsManager\SmsMessage;
use Illuminate\Notifications\Notification;

class RightNotification extends Notification
{

	public function via($notifiable)
	{
		return [SmsManagerChannel::class];
	}

	public function toSmsManager($notifiable): SmsMessage
	{
		return new SmsMessage('Test sms message');
	}
}
