
#Simple guide:

- composer abetzi/laravel-smsmanager
- nainstalovat knihovnu

## Configuration

~~~shell
artisan vendor:publish --provider=Abetzi\\SmsManager\\SmsManagerServiceProvider
~~~

- editovat **.env**

~~~
SMSMANAGER_APIKEY=26173e6e84ab1d8c77a8b325d9a39c265bfe6a5a3
SMSMANAGER_HTTPAPI=https://http-api.smsmanager.cz
SMSMANAGER_TIMEOUT=30
~~~


- do tridy **User** doplnit metodu **routeNotificationForSmsManager** => to zajisti zjisteni telefonniho cisla

~~~php
<?php

class User extends Authenticatable
{
     
	use Notifiable;
     
	/**
     * Returns route for notification - phone number for sending sms 
     * 
	 * @return string
 	 */
	public function routeNotificationForSmsManager(){
		
		return '420601123456';	    
	}
}
~~~

## Usage

- do prislusne notifikacni tridy dat metodu toSmsManager() => popise co a jak se ma poslat (gateway)

~~~php
<?php

class InvoiceReminder extends Notification
{

        use Queueable;

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed $notifiable
         *
         * @return array
         */
        public function via($notifiable)
        {
                return [SmsManagerChannel::class];
        }

		/**
		 * @param mixed $notifiable
 		 *                         
 		 * @return SmsMessage
 		 */
        public function toSmsManager($notifiable): SmsMessage
        {
                return new SmsMessage('Hello, this is invoice remainder, do not forgot to pay.');
        }
}
~~~

- send a message

~~~php
<?php

$notifiable   = User::find(123);
$notification = new InvoiceReminder();

$notifiable->notify($notification);
~~~

- or you can use Notification facade ([more]())

~~~php
<?php

$notification = new InvoiceReminder();

Notification::route(SmsManagerChannel::class, '420603123456')->notify($notification);
~~~
