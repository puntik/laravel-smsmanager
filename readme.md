# abetzi/laravel-smsmanager

[![Latest Stable Version](https://poser.pugx.org/abetzi/laravel-smsmanager/v/stable)](https://packagist.org/packages/abetzi/laravel-smsmanager)
[![Total Downloads](https://poser.pugx.org/abetzi/laravel-smsmanager/downloads)](https://packagist.org/packages/abetzi/laravel-smsmanager)

## Instalation

~~~shell
composer require abetzi/laravel-smsmanager
~~~

## Documentation

 Learn more in the [documentation](docs/index.md).

## Tests

~~~sh
$ ./vendor/bin/phpunit
~~~

## Todos:

- sender parameter missing for gateway high
- error states from server ([more](https://smsmanager.cz/api/codes/#errors))
- lumen
